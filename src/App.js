import React, { Component } from "react";
import IztegliSi from "./IztegliSi.jsx";

class App extends Component {
  render() {
    return (
      <div className="App">
        <ul className="header">
          <li>ИзтеглиСи</li>
        </ul>

        <IztegliSi />
      </div>
    );
  }
}

export default App;
