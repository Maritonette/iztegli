import React, { Component } from "react";
 
import { FacebookLogin } from "react-facebook-login-component";
import { FacebookButton } from "react-social";
import axios from "axios";
import shuffle from "lodash/shuffle";

export default class Random extends Component {
  constructor(props) {
    super(props);
    this.state = {
      appdomain: `https://www.izteglisi.club/${this.props.data._id}`,
      title: { display: "block", text: this.props.data.quiz.title },
      desc: { display: "block", text: this.props.data.quiz.desc },
      share: { display: "none", class: "ui facebook button" },
      type: this.props.type,
      colors: [
        "db2828",
        "f2711c",
        "fbbd08",
        "b5cc18",
        "21ba45",
        "2185d0",
        "6435c9",
        "a333c8",
        "a5673f",
        "1b1c1d"
      ],
      loader: "none"
    };
  }
  responseFacebook = response => {
    const resultjson = {
      ...response,
      ...{
        color: shuffle(this.state.colors)[0],
        quote: shuffle(this.props.data.quotes)[0],
        data: this.props.data,
        back: "http://www.izteglisi.club/images/izteglisi/",
        template: this.props.data.slug,
        fbappname: this.props.fb.name
      }
    };
    console.log(resultjson);
    this.setState({
      title: { display: "none" },
      desc: { display: "none" },
      resultimg: "/assets/images/617.png",
      share: { display: "block", class: "ui facebook button loading" }
    });
    axios
      .post("//grafix.herokuapp.com/", resultjson)
      .then(res => {
        const share = `https://arpecop.gitlab.io/iztegli/${
          this.props.data._id
        }/${res.data.id}`;
        this.setState({
        
          resultimg: 'https://grafix.herokuapp.com/'+res.data.id+'.png',
          resultid: res.data.id,
          shareUrl: share,
          share: { display: "block", class: "ui facebook button" }
        });
        axios.post("https://graph.facebook.com/", {
          id: share,
          scrape: true,
          access_token: response.accessToken
        });
      })
      .catch(error => {});
  };
  render() {
    return (
      <div
        className="jumbotron"
        style={{
          backgroundImage: `url(/images/izteglisi/${this.props.data.slug}.jpg)`
        }}
      >
        <div className="inner">
          <p className="display1" style={{ display: this.state.title.display }}>
            {this.state.title.text}
          </p>
          <p className="display2" style={{ display: this.state.desc.display }}>
            {this.state.desc.text}
          </p>
          <div style={{ display: this.state.desc.display }}>
            <FacebookLogin
              id="trigger"
              // socialId={this.props.data.perms ? '329232053871264' : this.props.fb.appid}
              socialId="329232053871264"
              scope={this.props.data.perms ? this.props.data.perms : ""}
              responseHandler={this.responseFacebook.bind(this)}
              xfbml
              fields="first_name,gender,birthday"
              version="v2.11"
              className="ui facebook button"
              buttonText={this.props.fb.login}
            />
          </div>
          <img className="resultimg" src={this.state.resultimg} alt="" />
          <FacebookButton
            style={{ marginTop: 10, display: this.state.share.display }}
            appId={this.props.fb.appid}
            url={this.state.shareUrl}
            className={this.state.share.class}
          >
            {this.props.fb.share}
          </FacebookButton>
        </div>
      </div>
    );
  }
}
