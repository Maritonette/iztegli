function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
;var locals_for_with = (locals || {});(function (all, picture, quiz, undefined) {
var jade_indent = [];
buf.push("<!DOCTYPE html>\n<html lang=\"en\">\n  <head>\n    <style>\n      @font-face {\n      font-family: \"Open Sans\";\n      src: url(\"//s3-eu-west-1.amazonaws.com/arpecop.com/cdn/OpenSans-Bold.woff\") format('woff');\n      }\n    </style>\n    <meta charset=\"utf-8\">\n  </head>\n  <body style=\"margin:0;padding:0;\"> \n    <div" + (jade.attr("style", "text-align:center; height:335px; width:620px; font-family:'Open Sans'; background: transparent url('"+picture.data.url+"')  no-repeat center center;      -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;   font-weight:700;", true, true)) + " class=\"coverimage\">\n      <div style=\"color:#FFF; -webkit-font-smoothing: antialiased; -webkit-border-radius:4px; padding:5px;   max-width:80%;   \" class=\"text\">" + (((jade_interp = quiz.title) == null ? '' : jade_interp)) + "</div>");
// iterate all
;(function(){
  var $$obj = all;
  if ('number' == typeof $$obj.length) {

    for (var i = 0, $$l = $$obj.length; i < $$l; i++) {
      var fr = $$obj[i];

buf.push("<img" + (jade.attr("src", fr.picture.data.url, true, true)) + " alt=\"alt\" height=\"150\" style=\"border-radius: 75px;margin:4px; \">");
    }

  } else {
    var $$l = 0;
    for (var i in $$obj) {
      $$l++;      var fr = $$obj[i];

buf.push("<img" + (jade.attr("src", fr.picture.data.url, true, true)) + " alt=\"alt\" height=\"150\" style=\"border-radius: 75px;margin:4px; \">");
    }

  }
}).call(this);

buf.push("\n    </div>\n  </body>\n</html>");}.call(this,"all" in locals_for_with?locals_for_with.all:typeof all!=="undefined"?all:undefined,"picture" in locals_for_with?locals_for_with.picture:typeof picture!=="undefined"?picture:undefined,"quiz" in locals_for_with?locals_for_with.quiz:typeof quiz!=="undefined"?quiz:undefined,"undefined" in locals_for_with?locals_for_with.undefined:typeof undefined!=="undefined"?undefined:undefined));;return buf.join("");
}