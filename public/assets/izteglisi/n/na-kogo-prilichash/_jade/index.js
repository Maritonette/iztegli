function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
;var locals_for_with = (locals || {});(function (Math, f, first_name, image, m, picture) {
var jade_indent = [];
var mother = Math.floor((Math.random() * 100) + 0);
var father = Math.round(100 - mother)
buf.push("<!DOCTYPE html>\n<html lang=\"en\"> \n  <head>\n    <style>\n      @font-face {\n      font-family: \"Open Sans\";\n      src: url(\"//s3-eu-west-1.amazonaws.com/arpecop.com/cdn/OpenSans-Bold.woff\") format('woff');\n      }\n    </style>\n    <meta charset=\"utf-8\">\n  </head>\n  <body style=\"margin:0;padding:0;\"> \n    <div" + (jade.attr("style", "text-align:center; height:315px; width:"+mother+"%; font-family:'Open Sans'; background: transparent url('//s3-eu-west-1.amazonaws.com/arpecop.com/cdn/"+image+".jpg')  no-repeat center center;      -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover; display: table; float:left;", true, true)) + " class=\"mother\">");
if ( (mother > father))
{
buf.push("<img" + (jade.attr("src", picture.data.url, true, true)) + " alt=\"alt\" width=\"70px\">\n      <h2 id=\"father\">" + (jade.escape((jade_interp = first_name) == null ? '' : jade_interp)) + " " + (jade.escape((jade_interp = mother) == null ? '' : jade_interp)) + "% " + (jade.escape((jade_interp = m) == null ? '' : jade_interp)) + "</h2>");
}
buf.push("\n    </div>\n    <div" + (jade.attr("style", "text-align:center; height:315px; width:"+father+"%; font-family:'Open Sans'; background: transparent url('//s3-eu-west-1.amazonaws.com/arpecop.com/cdn/H10aTHwbx.jpg')  no-repeat center center;      -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover; display: table; float:left;", true, true)) + " class=\"father\">\n      <!--|#{first_name} -->");
if ( (father > mother))
{
buf.push("<img" + (jade.attr("src", picture.data.url, true, true)) + " alt=\"alt\" width=\"70px\">\n      <h2 id=\"father\">" + (jade.escape((jade_interp = first_name) == null ? '' : jade_interp)) + " " + (jade.escape((jade_interp = father) == null ? '' : jade_interp)) + "% " + (jade.escape((jade_interp = f) == null ? '' : jade_interp)) + "</h2>");
}
buf.push("\n    </div>\n  </body>\n</html>");}.call(this,"Math" in locals_for_with?locals_for_with.Math:typeof Math!=="undefined"?Math:undefined,"f" in locals_for_with?locals_for_with.f:typeof f!=="undefined"?f:undefined,"first_name" in locals_for_with?locals_for_with.first_name:typeof first_name!=="undefined"?first_name:undefined,"image" in locals_for_with?locals_for_with.image:typeof image!=="undefined"?image:undefined,"m" in locals_for_with?locals_for_with.m:typeof m!=="undefined"?m:undefined,"picture" in locals_for_with?locals_for_with.picture:typeof picture!=="undefined"?picture:undefined));;return buf.join("");
}