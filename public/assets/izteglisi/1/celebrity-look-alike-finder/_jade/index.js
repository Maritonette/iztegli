function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
;var locals_for_with = (locals || {});(function (Math, namex, ximg) {
var jade_indent = [];
var f = locals.quotes.female[Math.floor((Math.random() * 86) + 0)];
var f1 = f.replace('300','').replace('.jpg','').replace('female-','').replace('_','').replace('1','');
var m = locals.quotes.male[Math.floor((Math.random() * 65) + 0)];
var m1 = m.replace('300','').replace('.jpg','').replace('male-','').replace('_','').replace('1','');
buf.push("<!DOCTYPE html>\n<html lang=\"en\">\n  <head>\n    <meta charset=\"utf-8\">\n    <style>\n      @font-face {\n      font-family: \"OpenSans\";\n      src: url(\"//s3-eu-west-1.amazonaws.com/arpecop.com/cdn/OpenSans-Bold.woff\") format('woff');\n      }\n      @font-face {\n      font-family: \"big\";\n      src: url(\"//s3-eu-west-1.amazonaws.com/arpecop.com/cdn/OpenSans-Bold.woff\") format('woff');\n      }\n      .letter {\n      font-size: 20px;\n      text-shadow: 0px 0px 11px rgba(150, 150, 150, 1);\n      text-transform: uppercase;\n      font-family: 'Bold', Arial;\n      }\n      .letter div {\n      background-color:#582857;\n      margin-top:260px;\n      color:#FFF;\n      }\n    </style>\n  </head>\n  <body style=\"margin:0;padding:0;\"> \n    <div" + (jade.attr("style", "text-align:center; height:315px; width:50%; float:right; font-family:'OpenSans'; background: transparent url('"+ximg+"')  no-repeat center center;      -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover; display: table;", true, true)) + " class=\"coverimage1\"></div>\n  </body>");
if ( locals.gender == 'female')
{
buf.push("\n  <div" + (jade.attr("style", "text-align:center; height:315px; width:50%; float:right; font-family:'OpenSans'; background: transparent url('//s3-eu-west-1.amazonaws.com/arpecop.com/cdn/"+f+"')  no-repeat center center;      -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover; display: table;", true, true)) + " class=\"letter\"> \n    <div>" + (jade.escape((jade_interp = namex) == null ? '' : jade_interp)) + " - " + (jade.escape((jade_interp = f1.replace('-',' ')) == null ? '' : jade_interp)) + "</div>\n  </div>");
}
else
{
buf.push("\n  <div" + (jade.attr("style", "text-align:center; height:315px; width:50%; float:right; font-family:'OpenSans'; background: transparent url('//s3-eu-west-1.amazonaws.com/arpecop.com/cdn/"+m+"')  no-repeat center center;      -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover; display: table;", true, true)) + " class=\"letter\"> \n    <div>" + (jade.escape((jade_interp = namex) == null ? '' : jade_interp)) + " - " + (jade.escape((jade_interp = m1.replace('-',' ')) == null ? '' : jade_interp)) + "</div>\n  </div>");
}
buf.push("\n</html>");}.call(this,"Math" in locals_for_with?locals_for_with.Math:typeof Math!=="undefined"?Math:undefined,"namex" in locals_for_with?locals_for_with.namex:typeof namex!=="undefined"?namex:undefined,"ximg" in locals_for_with?locals_for_with.ximg:typeof ximg!=="undefined"?ximg:undefined));;return buf.join("");
}