/* eslint-disable */
window.fbAsyncInit = function () {
  FB.init({
    appId: '122683342943',
    autoLogAppEvents: true,
    xfbml: true,
    version: 'v2.10',
  });
  FB.AppEvents.logPageView();
  FB.Canvas.setAutoGrow();
 
FB.getLoginStatus(function(response) {
  if (response.status === 'connected') {
    // the user is logged in and has authenticated your
    // app, and response.authResponse supplies
    // the user's ID, a valid access token, a signed
    // request, and the time the access token 
    // and signed request each expire
    var uid = response.authResponse.userID;
    var accessToken = response.authResponse.accessToken;
    document.getElementById("unsub").setAttribute("href", "http://sharlem.herokuapp.com/unsub/"+uid);
    FB.api('/me', {fields: 'first_name'}, function(response) {
       document.getElementById("username").innerHTML = response.first_name;
     }); 
  } else if (response.status === 'not_authorized') {
    // the user is logged in to Facebook, 
    // but has not authenticated your app
  } else {
    // the user isn't logged in to Facebook.
  }
 });

};

(function (d, s, id) {
  let js,
    fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) { return; }
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk.js';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

