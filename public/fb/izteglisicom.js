/* eslint-disable */
window.fbAsyncInit = function() {
  FB.init({
    appId: '329232053871264',
    autoLogAppEvents: true,
    xfbml: true,
    version: 'v2.10',
  });
  FB.AppEvents.logPageView();
  FB.Canvas.setAutoGrow();

  FB.getLoginStatus(function(response) {
    if (response.status === 'connected') {
      var uid = response.authResponse.userID;
      var accessToken = response.authResponse.accessToken;
      // document.getElementById("unsub").setAttribute("href", "http://sharlem.herokuapp.com/unsub/"+uid);
      FB.api('/me', { fields: 'first_name' }, function(response) {
        // document.getElementById("username").innerHTML = response.first_name;
      });
    } else if (response.status === 'not_authorized') {
      // the user is logged in to Facebook,
      // but has not authenticated your app
    } else {
      // the user isn't logged in to Facebook.
    }
  });
};

(function(d, s, id) {
  let js,
    fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) {
    return;
  }
  js = d.createElement(s);
  js.id = id;
  js.src = 'https://connect.facebook.net/bg_BG/sdk.js';
  fjs.parentNode.insertBefore(js, fjs);
})(document, 'script', 'facebook-jssdk');
